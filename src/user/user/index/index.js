import React, { Component } from 'react';
import requestTicket from '../../../services/ticketService';
import './index.css';

class UserIndex extends Component {
	 constructor(props) {
	    super(props);
	    this.state = {
	      user_id: '',
	      tickets: [],
	      loading: false
	    }
	    this.authUser();
	  }

    authUser(){
		let token = localStorage.getItem("jwt-token");
		fetch('http://localhost:3001/api/auth', {
			method: 'get',
			headers: {
				'Content-Type':'application/json',
				'Authorization': 'bearer '+token
			}
		})
		.then(response =>{
			if (!response.ok) {
				throw Error(response.statusText);
			}
			return response.json()
		}).then(res => {
			this.getTickets(res.id);
		});
	}

	getTickets(user_id){
		let token = localStorage.getItem("jwt-token");
		fetch('http://localhost:3001/api/tickets/user/'+user_id, {
			method: 'get',
			headers: {
				'Content-Type':'application/json',
				'Authorization': 'bearer '+token
			}
		})
		.then(response =>{
			if (!response.ok) {
				throw Error(response.statusText);
			}
			return response.json()
		}).then(res => {
			console.log(res)
			this.setState({
				user_id: user_id,
				tickets: res,
				loading: true
			});
		});
	}
	checkStatus(ticket_requested){
		if(ticket_requested == 0){
			return 'Pendiente';
		}else{
			return 'Aprobada';
		}
	}

	setTicket = () => {
		let user_id = this.state.user_id;
		requestTicket(user_id);
		this.setState({
				loading: false
			});
		this.authUser();
        console.log('ticket seteado')
    }

	render() {
		if (this.state.loading)
		      return this.showTable()
		    else
		      return (
		      	<div className="container">
		      		<button onClick={this.setTicket} className="btn btn-secondary">
					   Generar ticket
					</button>
					<br/>
		      		Cargando datos...
		      	</div>)
		  }
		showTable(){
			return (
				<div className="container">
					<button onClick={this.setTicket} className="btn btn-secondary">
					   Generar ticket
					</button>
					<table className="table table-hover">
					  <thead className="table-light">
					    <tr>
					      <th scope="col">N° de ticket</th>
					      <th scope="col">Estado de la solicitud</th>
					    </tr>
					  </thead>
					  <tbody>
					    {
					    	this.state.tickets.map(tick => {
					            return (
					              <tr key={tick.id}>
					                <td>{tick.id}</td>
					                <td>{
					                		this.checkStatus(tick.ticket_requested)
					                	}</td>
					              </tr>
					            );
					          })
					    }
					  </tbody>
					</table>
				</div>
				);
		}
	}


export default UserIndex;