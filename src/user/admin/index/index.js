import React, { Component } from 'react';
import './index.css';
import check from '../../../assets/check.svg';
import cancel from '../../../assets/cancel.svg';

class AdminIndex extends Component {
	constructor(props) {
	    super(props);
	    this.state = {
	      tickets: [],
	      loading: false
	    }
	    this.getTickets();
	  }
	  getTickets(){
	  	let token = localStorage.getItem("jwt-token");
		fetch('http://localhost:3001/api/tickets/', {
			method: 'get',
			headers: {
				'Content-Type':'application/json',
				'Authorization': 'bearer '+token
			}
		})
		.then(response =>{
			if (!response.ok) {
				throw Error(response.statusText);
			}
			return response.json()
		}).then(res => {
			console.log(res)
			this.setState({
				tickets: res,
				loading: true
			});
		});
	  }
	  setTicket = (id_ticket) => {
	  	let token = localStorage.getItem("jwt-token");
		fetch('http://localhost:3001/api/tickets/'+id_ticket, {
			method: 'put',
			headers: {
				'Content-Type':'application/json',
				'Authorization': 'bearer '+token
			}
		})
		.then(response =>{
			if (!response.ok) {
				throw Error(response.statusText);
			}
			return response.json()
		}).then(res => {
			console.log(res)
			this.setState({
				loading: false
			});
		});
	  	this.getTickets();
	  }

	  deleteTicket = (id_ticket) => {
	  	let token = localStorage.getItem("jwt-token");
		fetch('http://localhost:3001/api/tickets/'+id_ticket, {
			method: 'delete',
			headers: {
				'Content-Type':'application/json',
				'Authorization': 'bearer '+token
			}
		})
		.then(response =>{
			if (!response.ok) {
				throw Error(response.statusText);
			}
			return response.json()
		}).then(res => {
			console.log(res)
			this.setState({
				loading: false
			});
		});
	  	this.getTickets();
	  }
	render() {
		if (this.state.loading)
			if(this.state.tickets.length != 0)
		      return this.showTable()
		  	else
		  		return (
		      	<div className="container">
		      		No hay nuevas solicitudes
		      	</div>)
		   else
		      return (
		      	<div className="container">
		      		Cargando datos...
		      	</div>)
		  }
		showTable(){
			return (
				<div className="container">
					<table className="table table-hover">
					  <thead className="table-light">
					    <tr>
					      <th scope="col">N° de ticket</th>
					      <th scope="col">Usuario</th>
					      <th scope="col">Correo</th>
					      <th scope="col">Opciones</th>
					    </tr>
					  </thead>
					  <tbody>
					  {
					    	this.state.tickets.map(tick => {
					            return (
					              <tr key={tick.id}>
					                <td>{tick.id}</td>
					                <td>{tick.name}</td>
					                <td>{tick.mail}</td>
					                <td>
								      	<button onClick={this.setTicket.bind(this, tick.id)} className="btn btn-secondary">
								      		<img src={ check } /> 
								      	</button>
								      	<button onClick={this.deleteTicket.bind(this, tick.id)} className="btn btn-secondary">
								      		<img src={ cancel } /> 
								      	</button>
								     </td>
					              </tr>
					            );
					          })
					    }
					  </tbody>
					</table>
				</div>
				);
		}
	}

export default AdminIndex;