import React, { Component } from 'react';
import './login.css';
import user_lock from '../../assets/user_lock.svg';
import loginData from '../../services/loginService';

class Login extends Component {
	 state ={
        mail : '',
        pass: ''
    }
    handleChange = (e) => {
        this.setState({
            [e.target.id]: e.target.value
        })
    }
    handleSubmmit = (e) => {
        e.preventDefault();
        loginData(this.state);
    }
	render() {
		return (
			<div className="login-form">
				<form onSubmit={this.handleSubmmit}>
					<img src={ user_lock } />      
					<div className="form-group">
						<input 
							type="email" 
							className="form-control" 
							id="mail" 
							placeholder="Correo" 
							required="required" 
							onChange={this.handleChange} />
					</div>
					<div className="form-group">
						<input 
							type="password" 
							className="form-control" 
							id="pass" 
							placeholder="Contraseña" 
							required="required" 
							onChange={this.handleChange} />
					</div>
					<div className="form-group">
						<button 
							type="submit" 
							className="btn btn-primary btn-block">
							Iniciar sesión
						</button>
					</div>       
				</form>
				<div className="text-center">
					Regístrate
					<a href="/sign_up">aquí</a>
				</div>
			</div>
			);
	}
}

export default Login;