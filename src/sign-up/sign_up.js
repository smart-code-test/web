import React, { Component } from 'react';
import signUpData from '../services/signUpService';
import './sign_up.css';

class SignUp extends Component {
	  constructor(props){
    super(props);
    console.log(props)
  }
	state ={
		name: '',
        mail : '',
        pass: '',
        pass2: '',
        id_user_type: ''
    }
    handleChange = (e) => {
        this.setState({
            [e.target.id]: e.target.value
        })
    }
    handleSubmmit = (e) => {
        e.preventDefault();
        let pass1 = this.state.pass;
        let pass2 = this.state.pass2;
        this.validatePass(pass1,pass2)
    }
    validatePass = (pass1,pass2) => {
    	if(pass1 !== pass2){
    		return console.log('Las contraseñas no coinciden');
    	}else{
    		signUpData(this.state);
    	}
    }
	render() {
		return (
			<div>
				<div className="signup-form">
					<form onSubmit={this.handleSubmmit}>
						<div className="form-group">
							<input type="text" 
							className="form-control" 
							name="name" 
							placeholder="Nombre" 
							id="name"
							required="required" 
							onChange={this.handleChange}/>
						</div>
						<div className="form-group">
							<input 
							type="email" 
							className="form-control" 
							name="email" 
							placeholder="Correo" 
							id="mail"
							required="required" 
							onChange={this.handleChange}/>
						</div>
						<div className="form-group">
							<input 
							type="password" 
							className="form-control" 
							name="password" 
							placeholder="Contraseña" 
							id="pass"
							required="required" 
							onChange={this.handleChange}/>
						</div>
						<div className="form-group">
							<input 
							type="password" 
							className="form-control" 
							name="confirm_password" 
							placeholder="Confirme contraseña"
							id="pass2" 
							required="required" 
							onChange={this.handleChange}/>
						</div>
						<div className="form-group">
						<fieldset className="form-group">
						<span>Rol</span>
					      <div className="form-check">
					        <label className="form-check-label">
					          <input 
					          type="radio" 
					          className="form-check-input" 
					          name="optionsRadios" 
					          id="id_user_type" 
					          value="2"
					          onChange={this.handleChange} 
					          required/>Usuario
					        </label>
					      </div>
					      <div className="form-check">
					        <label className="form-check-label">
					          <input 
					          type="radio" 
					          className="form-check-input" 
					          name="optionsRadios" 
					          id="id_user_type"
					          value="1" 
					          onChange={this.handleChange}/>Administrador
					       </label>
					      </div>
					     </fieldset>
					    </div>
						<div className="form-group">
							<button 
							className="btn btn-primary btn-lg btn-block">Registrarse</button>
						</div>
					</form>
					<div className="text-center">
						Ya tiene una cuenta?
						<a href="/login">Inicie sesión</a>
					</div>
				</div>
			</div>
			);
		}
	}

	export default SignUp;