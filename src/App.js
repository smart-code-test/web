import React from 'react';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from 'react-router-dom';
//Components
import Header from './shared/header/header';
import Login from './auth/login/login';
import Sign_up from './sign-up/sign_up';
import AdminIndex from './user/admin/index/index';
import UserIndex from './user/user/index/index';


function App() {

  let header;
  let getToken = localStorage.getItem("jwt-token");

   if(getToken){
     header = <Header />;
   }

  return (
    <div className="background">
      <Router>
        {header}
        <div className="container">
          <Switch>
            <Route path="/login">
              <Login />
            </Route>
            <Route path="/sign_up">
              <Sign_up />
            </Route>
            <Route path="/admin">
              <AdminIndex />
            </Route>
            <Route path="/user">
              <UserIndex />
            </Route>
            <Route path="*">
              <Redirect to="/login" />
            </Route> 
          </Switch>
        </div>
      </Router>
    </div>
    );
}

export default App;
