function signUpData(data){
	fetch('http://localhost:3001/api/user/sign_up', {
		method: 'post',
		headers: {'Content-Type':'application/json'},
		body: JSON.stringify(
		{
			"id_user_type": data.id_user_type,
			"name": data.name,
			"mail": data.mail,
			"pass": data.pass
		})
	})
	.then(response =>{
		if (!response.ok) {
			throw Error(response.statusText);
		}
		return response.json()
	}).then(res => {
		alert(res.msj)
		window.location = "/login";
	});
}

export default signUpData;