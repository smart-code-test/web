function requestTicket(user_id){
	let token = localStorage.getItem("jwt-token");
	fetch('http://localhost:3001/api/tickets/request', {
		method: 'post',
		headers: {
			'Content-Type':'application/json',
			'Authorization': 'bearer '+token
		},
		body: JSON.stringify(
		{
			"id_user": user_id
		}) 
	})
	.then(response =>{
		if (!response.ok) {
			throw Error(response.statusText);
		}
		return response.json()
	}).then(res => {
		console.log(res)
		
	});
}

export default requestTicket;