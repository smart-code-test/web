function loginData(data){
	fetch('http://localhost:3001/api/auth/login', {
		method: 'post',
		headers: {'Content-Type':'application/json'},
		body: JSON.stringify(
		{
			"mail": data.mail,
			"pass": data.pass
		}) 
	})
	.then(response =>{
		if (!response.ok) {
			throw Error(response.statusText);
		}
		return response.json()
	}).then(res => {
		console.log(res)
		if(res.token !== undefined){ 
			setToken(res.token);
			redirect(res.user_type);
		}else{
			alert(res.error)
		}
	});
}

	//set token
	const setToken = (token) => {
		localStorage.setItem("jwt-token", token);
	}
	//redirect
	const redirect = (user_id) => {
		user_id === 'admin' ? window.location ='/admin' : window.location ='/user';
	}

export default loginData;