import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './header.css';

class Header extends Component {
  closeSession = (e) => {
    e.preventDefault();
    localStorage.removeItem("jwt-token");
    window.location = "/login";
  }
  render() {
    return (
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <div className="collapse navbar-collapse" id="navbarColor02">
          <ul className="navbar-nav mr-auto">
          </ul>
          <form className="form-inline my-2 my-lg-0">
            <button className="btn btn-outline-secondary" onClick={this.closeSession}>Cerrar sesión</button>
          </form>
        </div>
      </nav>
      );
  }
}

export default Header;

